package az.ibar.stockapp.repository;

import az.ibar.stockapp.entity.CustomerAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerAccountRepository extends JpaRepository<CustomerAccount,Long> {
    CustomerAccount findByAccountNumber(String accountNumber);
}
