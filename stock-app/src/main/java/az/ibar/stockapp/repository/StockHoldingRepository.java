package az.ibar.stockapp.repository;

import az.ibar.stockapp.entity.StockHolding;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockHoldingRepository extends JpaRepository<StockHolding,Long> {

}
