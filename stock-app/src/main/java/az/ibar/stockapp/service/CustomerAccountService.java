package az.ibar.stockapp.service;


import az.ibar.stockapp.entity.CustomerAccount;
import az.ibar.stockapp.entity.User;
import az.ibar.stockapp.enums.BRStatus;
import az.ibar.stockapp.payload.CustomerAccountRequest;
import az.ibar.stockapp.repository.CustomerAccountRepository;
import az.ibar.stockapp.repository.UserRepository;
import az.ibar.stockapp.response.BaseResponse;
import az.ibar.stockapp.security.CurrentUser;
import az.ibar.stockapp.security.UserPrincipal;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomerAccountService {
    private CustomerAccountRepository customerAccountRepository;
    private UserRepository userRepository;

    public BaseResponse saveAccount(CustomerAccountRequest customerAccountRequest, UserPrincipal currentUser) {

        BaseResponse baseResponse = new BaseResponse(BRStatus.SUCCESS);

        CustomerAccount customerAccount = new CustomerAccount();
        customerAccount.setBalance(0.0);
        customerAccount.setCCY("AZN");
        customerAccount.setAccountNumber(customerAccountRequest.getAccountNumber());
        User user = userRepository.findByUsername(currentUser.getUsername()).get();
      //  customerAccount.setUser(user);
        baseResponse.setData(customerAccountRepository.save(customerAccount));
        return baseResponse;
    }


    public  CustomerAccount increase(String  account, Double amount){
        CustomerAccount customerAccount = customerAccountRepository.findByAccountNumber(account);
        customerAccount.setBalance(customerAccount.getBalance() + amount);
       return  customerAccountRepository.save(customerAccount);
    }

    public  CustomerAccount decrease(String  account, Double amount){
        CustomerAccount customerAccount = customerAccountRepository.findByAccountNumber(account);
        customerAccount.setBalance(customerAccount.getBalance() +- amount);
        return  customerAccountRepository.save(customerAccount);
    }

    public BaseResponse increaseBalance(String  account, Double amount) {
        BaseResponse baseResponse = new BaseResponse(BRStatus.SUCCESS);
        baseResponse.setData(increase(account,amount));
        return baseResponse;
    }

    public BaseResponse decreaseBalance(String account, Double amount) {
        BaseResponse baseResponse = new BaseResponse(BRStatus.SUCCESS);
        baseResponse.setData(decrease(account,amount));
        return baseResponse;
    }


}
