package az.ibar.stockapp.service;


import az.ibar.stockapp.entity.Stock;
import az.ibar.stockapp.entity.StockHolding;
import az.ibar.stockapp.entity.User;
import az.ibar.stockapp.enums.BRStatus;
import az.ibar.stockapp.repository.StockHoldingRepository;
import az.ibar.stockapp.repository.StockRepository;
import az.ibar.stockapp.repository.UserRepository;
import az.ibar.stockapp.response.BaseResponse;
import az.ibar.stockapp.security.UserPrincipal;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class StockService {
    private StockRepository stockRepository;
    private StockHoldingRepository stockHoldingRepository;
    private UserRepository userRepository;
    private CustomerAccountService customerAccountService;
    private EmailService emailService;

    public BaseResponse getStockList() {
        BaseResponse baseResponse = new BaseResponse(BRStatus.SUCCESS);
        baseResponse.setData(stockRepository.findAll());
        return baseResponse;
    }

    public BaseResponse buyStock(String symbol, int numberOfShares, UserPrincipal userPrincipal) {
        BaseResponse baseResponse = new BaseResponse(BRStatus.SUCCESS);
        User user = userRepository.findByUsername(userPrincipal.getUsername()).get();
        Stock stock = stockRepository.findBySymbol(symbol);
        StockHolding stockHolding = StockHolding.buyShares(user, symbol, numberOfShares, stock.getPrice());
        stockHoldingRepository.save(stockHolding);
//        customerAccountService.decrease(user.getCustomerAccounts().getAccountNumber(), stock.getPrice());
//        emailService.sendMail(user.getEmail(),"BUY-"+stock.getSymbol(),
//                "You bought stock : "+stock.getSymbol()+",price : "+stock.getPrice() +
//                        ", numberOfShares : "+numberOfShares+", Current balance is  "+user.getCustomerAccounts().getBalance());
        baseResponse.setData(stockHoldingRepository.save(stockHolding));
        return baseResponse;
    }

    public BaseResponse sellStock(String symbol, int numberOfShares, UserPrincipal userPrincipal) {
        BaseResponse baseResponse = new BaseResponse(BRStatus.SUCCESS);
        User user = userRepository.findByUsername(userPrincipal.getUsername()).get();
        Stock stock = stockRepository.findBySymbol(symbol);
        StockHolding stockHolding = StockHolding.buyShares(user, symbol, numberOfShares, stock.getPrice());
        stockHoldingRepository.save(stockHolding);
      //  customerAccountService.increase(user.getCustomerAccounts().getAccountNumber(), stock.getPrice());
//        emailService.sendMail(user.getEmail(),"BUY-"+stock.getSymbol(),
//                "You bought stock : "+stock.getSymbol()+",price : "+stock.getPrice() +
//                        ", numberOfShares : "+numberOfShares+", Current balance is  "+user.getCustomerAccounts().getBalance());

        baseResponse.setData(stockHoldingRepository.save(stockHolding));
        return baseResponse;
    }
}
