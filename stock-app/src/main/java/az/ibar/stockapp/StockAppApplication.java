package az.ibar.stockapp;

import az.ibar.stockapp.entity.CustomerAccount;
import az.ibar.stockapp.entity.Role;
import az.ibar.stockapp.entity.RoleName;
import az.ibar.stockapp.entity.User;
import az.ibar.stockapp.repository.CustomerAccountRepository;
import az.ibar.stockapp.repository.UserRepository;
import az.ibar.stockapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class StockAppApplication implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;
    @Autowired
    CustomerAccountRepository customerAccountRepository;
    @Autowired
    PasswordEncoder bCryptPasswordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(StockAppApplication.class, args);
    }


    @Transactional
    @Override
    public void run(String... args) throws Exception {
        Role role=new Role();
        role.setName(RoleName.ROLE_USER);

        Set<Role> roleSet=new HashSet<>();
        roleSet.add(role);

        User user=new User();
        user.setRoles(roleSet);
        user.setPassword("test123");
        user.setIsActive(1);
        user.setUsername("anar");
        user.setEmail("anar@google.com");
        user.setName("Anar Xocayev");
        CustomerAccount customerAccount=new CustomerAccount();
        customerAccount.setAccountNumber("AZN2345");
        customerAccount.setBalance(1000.00);
        customerAccount.setCCY("AZN");
     //  customerAccount = customerAccountRepository.save(customerAccount);
        System.out.println(customerAccount);
       // user.setCustomerAccounts(customerAccount);
        user.setConfirmationToken("test");
        //userRepository.save(user);

        System.out.println(bCryptPasswordEncoder.encode("test12345"));

    }
}
