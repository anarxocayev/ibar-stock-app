package az.ibar.stockapp.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "transactions")
@Data
@JsonIgnoreProperties("stockHolding")
public class StockTransaction extends BaseEntity {
    public enum TransactionType {
        BUY("buy"), SELL("sell");
        @SuppressWarnings("unused")
        private String type;

        TransactionType(String type) {
            this.type = type;
        }
    }

    private TransactionType type;
    private Integer shares;
    private Double price;
    private Date transactionTime;
    private String symbol;
    private Long userId;

    @ManyToOne
    @ToString.Exclude
    private StockHolding stockHolding;
    public StockTransaction(){}
    public StockTransaction(StockHolding stockHolding, int shares, TransactionType type,Double price) {
        this.shares = shares;
        this.stockHolding = stockHolding;
        this.transactionTime = new Date();
        this.symbol = stockHolding.getSymbol();
        this.type = type;
        this.userId = stockHolding.getOwnerId();
        this.price =price;
    }


}
