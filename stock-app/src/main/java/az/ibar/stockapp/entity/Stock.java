package az.ibar.stockapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "stocks")
@Data
@JsonIgnoreProperties({"createdAt", "updatedAt"})
public class Stock extends BaseEntity {
    private String symbol;
    private String company;
    private Double price;
}
