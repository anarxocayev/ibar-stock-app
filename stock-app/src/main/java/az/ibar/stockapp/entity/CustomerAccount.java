package az.ibar.stockapp.entity;

import lombok.Data;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@Table(name = "customer_accounts",
        uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "accountNumber"
        }),

})
public class CustomerAccount extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String accountNumber;
    @NotBlank
    private String CCY;

    private Double balance;
/*
    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @ToString.Exclude
    private User user;*/

}
