package az.ibar.stockapp.controller;


import az.ibar.stockapp.payload.CustomerAccountRequest;
import az.ibar.stockapp.response.BaseResponse;
import az.ibar.stockapp.security.CurrentUser;
import az.ibar.stockapp.security.UserPrincipal;
import az.ibar.stockapp.service.CustomerAccountService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/account")
@AllArgsConstructor
@Slf4j
public class CustomerAccountController {

    private CustomerAccountService customerAccountService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @CrossOrigin
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = {@Authorization(value = "jwtToken")})
    public BaseResponse saveAccount(@RequestBody CustomerAccountRequest customerAccountRequest,
                                    @CurrentUser UserPrincipal currentUser) {
        log.info("account payload {}", customerAccountRequest);
        BaseResponse baseResponse = customerAccountService.saveAccount(customerAccountRequest, currentUser);
        log.info("baseResponse {}", baseResponse);
        return baseResponse;

    }

    @RequestMapping(value = "/deposit", method = RequestMethod.POST)
    @CrossOrigin
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = {@Authorization(value = "jwtToken")})
    public BaseResponse increaseBalance(@RequestParam("accountNumber") String accountNumber,
                                        @RequestParam("amount") Double amount) {
        log.info("account  {},amount {}", accountNumber, amount);

        BaseResponse baseResponse = customerAccountService.increaseBalance(accountNumber, amount);
        log.info("baseResponse {}", baseResponse);
        return baseResponse;

    }

}
