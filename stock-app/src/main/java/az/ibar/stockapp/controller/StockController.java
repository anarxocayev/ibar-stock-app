package az.ibar.stockapp.controller;

import az.ibar.stockapp.response.BaseResponse;
import az.ibar.stockapp.security.CurrentUser;
import az.ibar.stockapp.security.UserPrincipal;
import az.ibar.stockapp.service.StockService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/stock")
@AllArgsConstructor
@Slf4j
public class StockController {
     private StockService stockService;

    @RequestMapping(value = "/stocks", method = RequestMethod.GET)
    @CrossOrigin
    public BaseResponse getStockList() {
        BaseResponse baseResponse = stockService.getStockList();
        log.info("baseResponse {}", baseResponse);
        return baseResponse;

    }

    @RequestMapping(value = "/buy", method = RequestMethod.GET)
    @CrossOrigin
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = {@Authorization(value = "jwtToken")})
    public BaseResponse buyStock(String symbol, int numberOfShares, @CurrentUser  UserPrincipal userPrincipal) {
        BaseResponse baseResponse = stockService.buyStock(symbol,numberOfShares,userPrincipal);
        log.info("baseResponse {}", baseResponse);
        return baseResponse;

    }
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = {@Authorization(value = "jwtToken")})
    @RequestMapping(value = "/sell", method = RequestMethod.GET)
    @CrossOrigin
    public BaseResponse sellStock(String symbol, int numberOfShares, @CurrentUser  UserPrincipal userPrincipal) {
        BaseResponse baseResponse = stockService.sellStock(symbol,numberOfShares,userPrincipal);
        log.info("baseResponse {}", baseResponse);
        return baseResponse;

    }
}
