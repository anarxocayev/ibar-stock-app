package az.ibar.stockapp.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerAccountRequest  {
    private Long id;
    private String accountNumber;
    private String CCY;
    private Double balance;
}
