package az.ibar.stockapp;

import az.ibar.stockapp.entity.User;
import az.ibar.stockapp.enums.BRStatus;
import az.ibar.stockapp.response.BaseResponse;
import az.ibar.stockapp.security.UserPrincipal;
import az.ibar.stockapp.service.EmailService;
import az.ibar.stockapp.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StockTesting {

    @Autowired
    private StockService stockService;

    @Test
    public void test_buy() {
        UserPrincipal user = new UserPrincipal(6L, "Anar Xocayev", "anarxocayev", "anarxocayev@gmail.com", "blabla", null);
        BaseResponse baseResponse = stockService.buyStock("TCDA", 2, user);
        assertThat(baseResponse.getStatus()).isEqualTo(BRStatus.SUCCESS);
    }

    @Test
    public void test_sell() {
        UserPrincipal user = new UserPrincipal(6L, "Anar Xocayev", "anarxocayev", "anarxocayev@gmail.com", "blabla", null);
        BaseResponse baseResponse = stockService.sellStock("TCDA", 2, user);
        assertThat(baseResponse.getStatus()).isEqualTo(BRStatus.SUCCESS);

    }

}
