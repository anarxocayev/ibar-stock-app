package az.ibar.stockapp;


import az.ibar.stockapp.controller.AuthController;
import az.ibar.stockapp.payload.ApiResponse;
import az.ibar.stockapp.payload.JwtAuthenticationResponse;
import az.ibar.stockapp.payload.LoginRequest;
import az.ibar.stockapp.payload.SignUpRequest;
import io.swagger.annotations.Api;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthControllerTesting {
    @Autowired
    TestRestTemplate testRestTemplate;


    @Test
    public void test_sign_up() throws Exception {
        SignUpRequest signUpRequest=new SignUpRequest();
        signUpRequest.setEmail("anarxocayev@gmail.com");
        signUpRequest.setName("Anar Xocayev");
        signUpRequest.setUsername("anarxocayev");
        signUpRequest.setPassword("test1234");


        ResponseEntity<ApiResponse> rspn =
                testRestTemplate.postForEntity("/api/auth/signup",signUpRequest, ApiResponse.class);

        assertThat(rspn.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(rspn.getBody().getMessage()).isEqualTo("User registered successfully");
        assertThat(rspn.getBody().getSuccess()).isEqualTo( true);

    }


    @Test
    public void test_sign_in() throws Exception {
        LoginRequest loginRequest=new LoginRequest();
        loginRequest.setPassword("test1234");
        loginRequest.setUsernameOrEmail("anarxocayev");


        ResponseEntity<JwtAuthenticationResponse> rspn =
                testRestTemplate.postForEntity("/api/auth/signin",loginRequest, JwtAuthenticationResponse.class);

        assertThat(rspn.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(rspn.getBody().getTokenType()).isEqualTo("Bearer");


    }

    @Test
    public void test_confirmation() throws Exception {
        ResponseEntity<ApiResponse> rspn =
                testRestTemplate.getForEntity("/api/auth/confirmation?confirmationToken=650d0c27-a384-452a-98be-a0a2ea802502", ApiResponse.class);
        assertThat(rspn.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(rspn.getBody().getSuccess()).isEqualTo(true);

    }
}

