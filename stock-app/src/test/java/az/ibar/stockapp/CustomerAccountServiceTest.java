package az.ibar.stockapp;

import az.ibar.stockapp.enums.BRStatus;
import az.ibar.stockapp.payload.CustomerAccountRequest;
import az.ibar.stockapp.response.BaseResponse;
import az.ibar.stockapp.security.CurrentUser;
import az.ibar.stockapp.security.UserPrincipal;
import az.ibar.stockapp.service.CustomerAccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerAccountServiceTest {
    @Autowired
    private CustomerAccountService customerAccountService;

    @Test
    public void test_open_account() {
        CustomerAccountRequest request=new CustomerAccountRequest();
        request.setAccountNumber("000AZE87637276");
        request.setBalance(0.0);
        request.setCCY("AZN");
        UserPrincipal currentUser=new UserPrincipal(1L,"Anar Xocayev","anarxocayev",
                "anarxocayev@gmail.com","test1234",null);

       BaseResponse baseResponse= customerAccountService.saveAccount(request,currentUser);
        assertThat(baseResponse.getStatus()).isEqualTo(BRStatus.SUCCESS);
    }

    @Test
    public void test_balance_increase() {
        BaseResponse baseResponse= customerAccountService.increaseBalance("000AZE87637276",89.0);
        assertThat(baseResponse.getStatus()).isEqualTo(BRStatus.SUCCESS);
    }
    @Test
    public void test_balance_decrease() {
        BaseResponse baseResponse= customerAccountService.decreaseBalance("000AZE87637276",8.0);
        assertThat(baseResponse.getStatus()).isEqualTo(BRStatus.SUCCESS);
    }

}
